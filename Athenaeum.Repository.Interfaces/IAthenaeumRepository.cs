﻿using Athenaeum.Domain.DataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Athenaeum.Repository.Interfaces
{
    public interface IAthenaeumRepository
    {
        public IEnumerable<Book> Books { get; }
    }
}
