﻿using Athenaeum.Domain.DataTypes;
using Athenaeum.Repository.Interfaces;
using Athenaeum.Repository.SqlServer.EFContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace Athenaeum.Repository.SqlServer
{
    public class SqlServerRepository : IAthenaeumRepository
    {
        private SqlServDbContext _context;

        public SqlServerRepository(SqlServDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<Book> Books {
            get => _context.Books;
        /*    get => new List<Book>()
            {
                new Book()
                {
                    BookInfoISBN = "isbn_example_1",
                    InventoryNumber = 1,
                },
                new Book()
                {
                    BookInfoISBN = "isbn_example_2",
                    InventoryNumber = 2,
                },
            };*/
        }
    }
}
