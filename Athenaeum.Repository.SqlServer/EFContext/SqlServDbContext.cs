﻿using Athenaeum.Domain.DataTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Athenaeum.Repository.SqlServer.EFContext
{
    public class SqlServDbContext : DbContext
    {
        public SqlServDbContext(DbContextOptions<SqlServDbContext> options) : base(options)
        {

        }

        public DbSet<Book> Books { get; set; }
        public DbSet<BookInfo> BookInfos { get; set; }
    }
}
