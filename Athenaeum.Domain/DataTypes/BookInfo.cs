﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Athenaeum.Domain.DataTypes
{
    public class BookInfo
    {
        [Key]
        public string ISBN { get; set; }

        public string Title { get; set; }


        public ICollection<Book> Books { get; set; }
    }
}
