﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Athenaeum.Domain.DataTypes
{
    public class Book
    {
        [Key]
        public int InventoryNumber { get; set; }

        public string BookInfoISBN { get; set; }


        public BookInfo BookInfo { get; set; }
    }
}
