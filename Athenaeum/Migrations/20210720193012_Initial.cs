﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Athenaeum.Web.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BookInfos",
                columns: table => new
                {
                    ISBN = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookInfos", x => x.ISBN);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    InventoryNumber = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BookInfoISBN = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.InventoryNumber);
                    table.ForeignKey(
                        name: "FK_Books_BookInfos_BookInfoISBN",
                        column: x => x.BookInfoISBN,
                        principalTable: "BookInfos",
                        principalColumn: "ISBN",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Books_BookInfoISBN",
                table: "Books",
                column: "BookInfoISBN");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "BookInfos");
        }
    }
}
